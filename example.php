<?php

require_once __DIR__ . '/vendor/autoload.php';

use Forrence\Miscellaneous\WordSearchGenerator;

$generator = new WordSearchGenerator;

$words = $generator->transformList([
    'Belkar',
    'Roy',
    'Elan',
    'Vaarsuvius',
    'Haley',
    'Durkon',
]);
$grid = $generator->generateInitialGrid($words, 7);

foreach ($words as $word) {
    $grid = $generator->fillGridWithWord($grid, $word);
}

echo 'Number of open letters: ' . $generator->countEmpties($grid) . PHP_EOL;

$grid = $generator->fillEmpties($grid, 'ThisIsAHiddenMessage');

echo $generator->printGrid($grid, "");
