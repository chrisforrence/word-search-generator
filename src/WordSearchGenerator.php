<?php

namespace Forrence\Miscellaneous;

class WordSearchGenerator
{
    public $directions = [
        0 => [0, -1],
        1 => [1, -1],
        2 => [1, 0],
        3 => [1, 1],
        4 => [0, 1],
        5 => [-1, 1],
        6 => [-1, 0],
        7 => [-1, -1],
    ];

    public function transformList($list) {
        array_walk($list, function(&$word) {
            $word = str_replace(' ', '', strtoupper($word));
        });
        usort($list, function($a, $b) {
            return strlen($b) <=> strlen($a);
        });
        return $list;
    }

    public function generateInitialGrid($list, $minimum_size = 0)
    {
        $minimum_list = array_reduce($list, function($carry, $word) {
            $carry = max($carry, strlen($word));
            return $carry;
        }, 0);

        $total_letters = array_reduce($list, function($carry, $word) {
            $carry += strlen($word);
            return $carry;
        }, 0);

        $minimum_size = max($minimum_list, ceil(sqrt($total_letters)), $minimum_size);

        $grid = array_fill(0, $minimum_size, array_fill(0, $minimum_size, null));

        return $grid;
    }

    public function fillGridWithWord($grid, $word) {
        $eligible = [];
        for($x = 0; $x < count($grid); $x++) {
            for($y = 0; $y < count($grid[$x]); $y++) {
                if ($grid[$x][$y] === null) {
                    $eligible[] = [$x, $y];
                }
            }
        }
        $word = strtoupper($word);
        $wordlength = strlen($word);
        $filledIn = false;
        shuffle($eligible);

        do {
            $startingCoordinates = array_pop($eligible);
            $directions = range(0, 7);
            shuffle($directions);
            for ($z = 0; $z < count($directions); $z++) {
                $coordinateDirection = $this->direction($directions[$z]);
                $isEligible = true;
                for ($wl = 0; $wl < $wordlength; $wl++) {
                    $tempCoordinateX = $startingCoordinates[0] + ($coordinateDirection[0] * $wl);
                    $tempCoordinateY = $startingCoordinates[1] + ($coordinateDirection[1] * $wl);

                    if (!array_key_exists($tempCoordinateX, $grid) || !array_key_exists($tempCoordinateY, $grid[$tempCoordinateX]) || $grid[$tempCoordinateX][$tempCoordinateY] !== null) {
                        // We're either out of range or running into an already-taken
                        $isEligible = false;
                        break;
                    }
                }

                if ($isEligible) {
                    // Sweet! Fill in the grid and let's continue
                    for ($wl = 0; $wl < $wordlength; $wl++) {
                        $tempCoordinateX = $startingCoordinates[0] + ($coordinateDirection[0] * $wl);
                        $tempCoordinateY = $startingCoordinates[1] + ($coordinateDirection[1] * $wl);
                        $grid[$tempCoordinateX][$tempCoordinateY] = substr($word, $wl, 1);
                    }
                    return $grid;
                }
            }
        } while (count($eligible) > 0);

        throw new Exception("Could not generate a word search this time.");
    }

    public function direction($path) {
        return $this->directions[$path];
    }

    public function countEmpties($grid) {
        $response = 0;
        for ($x = 0; $x < count($grid); $x++) {
            for ($y = 0; $y < count($grid[$x]); $y++) {
                if ($grid[$x][$y] === null) {
                    $response++;
                }
            }
        }
        return $response;
    }

    public function fillEmpties($grid, $secretPhrase = '', $useRandomLetters = true) {
        $secretPhrase = strtoupper($secretPhrase);
        for ($x = 0; $x < count($grid); $x++) {
            for ($y = 0; $y < count($grid[$x]); $y++) {
                if ($grid[$x][$y] === null) {
                    if (strlen($secretPhrase)) {
                        $letter = substr($secretPhrase, 0, 1);
                        $secretPhrase = strlen($secretPhrase) > 1 ? substr($secretPhrase, 1) : '';
                    } else {
                        $letter = $useRandomLetters ? $this->quickRandom(1) : '?';
                    }
                    $grid[$x][$y] = $letter;
                }
            }
        }
        return $grid;
    }

    public function printGrid($grid, $separator = ' ') {
        $response = '';
        for ($x = 0; $x < count($grid); $x++) {
            for ($y = 0; $y < count($grid[$x]); $y++) {
                $response .= $grid[$x][$y] . $separator;
            }
            $response .= PHP_EOL;
        }
        return $response;
    }

    public function quickRandom($length = 16)
    {
        $pool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }
}